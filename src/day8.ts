import {List} from 'immutable';
import {sum} from './util';

export function day8a(input: string): number {
  const trees = prepareInput(input);

  const leftVisible = onlyVisibles(trees);
  const bottomVisible = flip(flip(flip(onlyVisibles(flip(trees)))));
  const rightVisible = flip(flip(onlyVisibles(flip(flip(trees)))));
  const topVisible = flip(onlyVisibles(flip(flip(flip(trees)))));

  return addNumbers(leftVisible, bottomVisible, rightVisible, topVisible).map(row => row.reduce(sum)).reduce(sum);
}

export function day8b(input: string): number {
  const trees = prepareInput(input);

  let max = 0

  for (let i = 0; i < trees.size; i++) {
    for (let j = 0; j < trees.get(0).size; j++) {
      max = Math.max(calculateView(j, i, trees), max);
    }
  }
  return max;
}

const calculateView = (x: number, y: number, trees: List<List<number>>): number => {
  const origin = trees.get(y).get(x);
  const left = nrVisibleTrees(trees.get(y).slice(x + 1), origin);
  const top = nrVisibleTrees(flip(trees).get(x).reverse().slice(y + 1), origin);
  const right = nrVisibleTrees(trees.get(y).reverse().slice(trees.size - x), origin);
  const bottom = nrVisibleTrees(flip(trees).get(x).slice(trees.size - y), origin);

  return left * right * top * bottom;
}

const addNumbers = (...matrices: List<List<number>>[]): List<List<number>> => {
  let result = List<List<number>>();
  for (let i = 0; i < matrices[0].get(0).size; i++) {
    let row = List.of<number>();
    for (let j = 0; j < matrices[1].get(0).size; j++) {
      row = row.push(matrices.map(matrix => matrix.get(i).get(j)).reduce((a, b) => a + b > 0 ? 1 : 0, 0));
    }
    result = result.push(row);
  }
  return result;
}

const onlyVisibles = (matrix: List<List<number>>): List<List<number>> => matrix.map(onlyVisiblesRow);
const onlyVisiblesRow = (row: List<number>): List<number> => row.map((nr, index, map) => nr > map.slice(0, index).max() || !index ? 1 : 0);
const nrVisibleTrees = (row: List<number>, height: number): number => {
  if (row.size == 0) {
    return 0;
  }
  if (row.get(0) >= height) {
    return 1;
  }
  return 1 + nrVisibleTrees(row.delete(0), height);
}

const flip = (matrix: List<List<number>>): List<List<number>> => matrix.get(0).map((val, index) => matrix.map(row => row.get(index)).reverse());
const prepareInput = (input: string): List<List<number>> => List(input.split('\n').filter(s => s)).map(row => List(row.split('').map(Number)));
