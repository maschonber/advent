import {List} from 'immutable';
import {sum} from './util';

interface Directory {
  path: string;
  size: number;
  parent: string;
}

interface State {
  directories: List<Directory>;
  position: string;
}

const initialState = {directories: undefined, position: undefined};

export function day7a(input: string): number {
  const dirState = prepareInput(input).reduce(reducerFunction, initialState);
  return dirState.directories.filter(dir => dir.size <= 100000).map(dir => dir.size).reduce(sum);
}

export function day7b(input: string): number {
  const result = prepareInput(input).reduce(reducerFunction, initialState);
  const toDelete = result.directories.find(dir => dir.path === '/').size - 40000000;
  return result.directories.filter(dir => dir.size > toDelete).minBy(dir => dir.size).size;
}

function reducerFunction(state: State, command: string): State {
  if (command.startsWith('$ cd /')) {
    return registerRoot()
  }

  if (command.startsWith('$ cd ..')) {
    return moveUp(state);
  }

  if (command.startsWith('$ cd')) {
    return moveDown(state, command);
  }

  if (command.startsWith('dir')) {
    return registerDirectory(state, command);
  }

  if (command.match(/^\d/)) {
    return updateFileSizes(state.directories.find(dir => dir.path === state.position), Number(command.slice(0, command.indexOf(' '))), state);
  }

  return state;
}

const moveUp = (state: State): State => ({...state, position: state.directories.find(dir => dir.path === state.position).parent});
const moveDown = (state: State, command: string) => ({...state, position: state.position + command.substr(5) + '/'});
const registerRoot = (): State => ({directories: List([{path: '/', size: 0, parent: undefined}]), position: '/'});
const registerDirectory = (state: State, command: string) => ({...state, directories: state.directories.push({path: state.position + command.substr(4) + '/', size: 0, parent: state.position})});

function updateFileSizes(toUpdate: Directory, fileSize: number, state: State): State {
  const updatedDirectory = {...toUpdate, size: toUpdate.size + fileSize};
  const without = state.directories.delete(state.directories.findIndex(dir => dir.path === toUpdate.path));
  const newState = {...state, directories: without.push(updatedDirectory)};

  if (!toUpdate.parent) {
    return newState;
  }
  return updateFileSizes(newState.directories.find(dir => dir.path === toUpdate.parent), fileSize, newState);
}

const prepareInput = (input: string): List<string> => List(input.split('\n').filter(s => s));
