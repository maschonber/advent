import {List, Repeat} from 'immutable';

interface Instruction {
  count: number,
  from: number,
  to: number,
}

interface State {
  stacks: List<List<string>>,
  instructions: List<Instruction>,
}

export const day5a = (input: string): string => getResult(applyInstructions(prepareInput(input), crateMover9000));
export const day5b = (input: string): string => getResult(applyInstructions(prepareInput(input), crateMover9001));

const getResult = (state: State): string => state.stacks.map(stack => stack.last()).join('');

function applyInstructions(input: State, moverFunction: Function): State {
  if (!input.instructions.first()) {
    return input;
  }
  return applyInstructions({instructions: input.instructions.delete(0), stacks: moverFunction(input.stacks, input.instructions.first())}, moverFunction);
}

const crateMover9000 = (stacks: List<List<string>>, instruction: Instruction): List<List<string>> => {
  for (let nr = 0; nr < instruction.count; nr++) {
    const toMove = stacks.get(instruction.from).last();
    stacks = stacks.set(instruction.from, stacks.get(instruction.from).delete(-1));
    stacks = stacks.set(instruction.to, stacks.get(instruction.to).push(toMove));
  }
  return stacks;
}

const crateMover9001 = (stacks: List<List<string>>, instruction: Instruction): List<List<string>> => {
  const toMove = stacks.get(instruction.from).slice(-instruction.count);
  stacks = stacks.set(instruction.from, stacks.get(instruction.from).slice(0, -instruction.count));
  stacks = stacks.set(instruction.to, stacks.get(instruction.to).push(...toMove.toJS()));
  return stacks;
}

function prepareInput(input: string): State {
  const lines = input.split('\n');
  const instructionLines = lines.slice(10).filter(s => s);
  const stackLines = List(lines.slice(0, 8).reverse().map(prepareLine));

  let stacks = List(Repeat(List.of<string>(), 9));

  stackLines.forEach((line) => line.forEach((char, index) => {
    if (char !== '.') {stacks = stacks.set(index, stacks.get(index).push(char))}
  }));

  const instructions = List(instructionLines.map(line => line.match('move (\\d+) from (\\d+) to (\\d+)'))
    .map(match => ({count: Number(match[1]), from: Number(match[2]) - 1, to: Number(match[3]) - 1})));

  return {stacks, instructions};
}

const prepareLine = (line: string): List<string> => List(line.replace(/\s{4}/g, '.').replace(/[\s\[\]]/g, '').split(''));
