import {Set} from 'immutable';

export function day6both(input: string, length: number): number {
  for (let i = 0; i < input.length; i++) {
    if (Set(input.substr(i - length + 1, length)).size == length) {
      return i + 1;
    }
  }
}