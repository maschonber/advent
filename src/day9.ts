import {List, Map, Repeat, Set} from 'immutable';

const movement: Map<string, Point> = Map({'D': {x: 0, y: -1}, 'U': {x: 0, y: 1}, 'L': {x: -1, y: 0}, 'R': {x: 1, y: 0}});

interface Point {x: number, y: number}
interface State {rope: List<Point>, tailLog: List<Point>}

export function day9both(input: string, length: number): number {
  const initialState = {rope: List(Repeat({x: 0, y: 0}, length)), tailLog: List.of({x: 0, y: 0})};
  return Set(prepareInput(input).reduce(reducerFunction, initialState).tailLog.map(stringify)).size;
}

const follow = (head: Point, tail: Point): Point => {
  const vector = getVector(head, tail);
  let target = tail;
  if (vector.x > 1 || (vector.x > 0 && Math.abs(vector.y) > 1)) {
    target = move(target, movement.get('R'));
  }
  if (vector.x < -1 || (vector.x < 0 && Math.abs(vector.y) > 1)) {
    target = move(target, movement.get('L'));
  }
  if (vector.y > 1 || (vector.y > 0 && Math.abs(vector.x) > 1)) {
    target = move(target, movement.get('U'));
  }
  if (vector.y < -1 || (vector.y < 0 && Math.abs(vector.x) > 1)) {
    target = move(target, movement.get('D'));
  }
  return target;
}

const move = (pos1: Point, pos2: Point): Point => ({x: pos1.x + pos2.x, y: pos1.y + pos2.y});
const getVector = (pos1: Point, pos2: Point): Point => ({x: pos1.x - pos2.x, y: pos1.y - pos2.y});
const stringify: (point) => string = point => point.x + '/' + point.y;

const reducerFunction = (state: State, command: string): State => {
  let newRope = List.of(move(state.rope.get(0), movement.get(command)));
  for (let i = 1; i < state.rope.size; i++) {
    newRope = newRope.push(follow(newRope.get(i-1), state.rope.get(i)));
  }
  return {tailLog: state.tailLog.push(newRope.last()), rope: newRope};
}

const prepareInput = (input: string): List<string> => List(input.split('\n').filter(s => s))
  .flatMap(line => Repeat(line.substr(0, 1), Number(line.substr(2))));
