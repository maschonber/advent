import {List} from 'immutable';
import {sum} from './util';

export const day1a = (input: string): number => elfList(input).max();
export const day1b = (input: string): number => elfList(input).sort().reverse().take(3).reduce(sum);

const splitByElf = (input: string) => List(input.split('\n\n'));
const sumFood = (elf: string) => elf.split('\n').map(Number).reduce(sum);

const elfList = (input: string): List<number> => splitByElf(input).map(sumFood);
