import {sum} from './util';
import {List} from 'immutable';

export const day4a = (input: string): number => prepareInput(input).map(containedFully).reduce(sum);
export const day4b = (input: string): number => prepareInput(input).map(containedPartially).reduce(sum);

const containedFully = (sections: List<number>): number => (sections.get(0) >= sections.get(2) && sections.get(1) <= sections.get(3))
  || (sections.get(2) >= sections.get(0) && sections.get(3) <= sections.get(1)) ? 1 : 0;

const containedPartially = (sections: List<number>): number => (sections.get(1) < sections.get(2) || sections.get(3) < sections.get(0)) ? 0 : 1

const prepareInput = (input: string): List<List<number>> => List(input.split('\n').filter(s => s).map(line => List(line.split(/[,|-]/).map(Number))));
