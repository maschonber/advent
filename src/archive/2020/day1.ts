import {List} from "immutable";

export function day1a(input: string): number {
    const numbers = prepareInput(input);

    for (let i of numbers) {
        for (let j of numbers) {
            if (i+j === 2020) {
                return i*j
            }
        }
    }
}

export function day1b(input: string): number {
    const numbers = prepareInput(input);

    for (let i of numbers) {
        for (let j of numbers) {
            for (let k of numbers) {
                if (i + j + k === 2020) {
                    return i * j * k
                }
            }
        }
    }
}

function prepareInput(input: string): List<number> {
    const strings = List(input.split('\n'))
    return strings.map(string => Number(string)).filter(nr => nr > 0);
}
