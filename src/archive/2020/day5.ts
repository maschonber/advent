import {Range} from 'immutable';

const sum = (a: number, b: number): number => a + b;
const getBinaryString = (input: string): string => input.replace(/[FL]/g, '0').replace(/[BR]/g, '1');
const getSeat = (binary: string): number => parseInt(binary.substr(0, 7), 2) * 8 + parseInt(binary.substr(7), 2);
const prepareInput = (input: string): number[] => input.split('\n').filter(n => n).map(getBinaryString).map(getSeat);
const findMissingSeat = (seats: number[]) => Range(Math.min(...seats), Math.max(...seats) + 1).reduce(sum, 0) - seats.reduce(sum, 0);

export const day5a = (input: string): number => Math.max(...prepareInput(input));
export const day5b = (input: string): number => findMissingSeat(prepareInput(input));