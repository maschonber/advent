import {List, Map} from 'immutable';

type Passport = Map<Prop, string>;
enum Prop {BIRTH = 'byr', ISSUE = 'iyr', EXPIRATION = 'eyr', HEIGHT = 'hgt', HAIR = 'hcl', EYE = 'ecl', ID = 'pid', COUNTRY = 'cid'}

const isNr = (value: string) => value.match(/^[0-9]*$/);
const isNrBetween = (min: number, max: number) => (value: string) => isNr(value) && Number(value) >= min && Number(value) <= max;
const isPassportId = (value: string) => isNr(value) && value.length === 9;

const heightMatches = (height: string, unit: string) =>
  (unit === 'in' && isNrBetween(59, 76)(height)) || (unit === 'cm' && isNrBetween(150, 193)(height));

const validateHeight = (value: string): boolean => {
  const matches = value.match(/^(\d+)(\w{2})$/);
  return matches && heightMatches(matches[1], matches[2]);
}

type Validation = (value: string) => boolean;
const validations = Map<Validation>({
  [Prop.BIRTH]: isNrBetween(1920, 2002),
  [Prop.ISSUE]: isNrBetween(2010, 2020),
  [Prop.EXPIRATION]: isNrBetween(2020, 2030),
  [Prop.ID]: isPassportId,
  [Prop.HEIGHT]: validateHeight,
  [Prop.HAIR]: /^#[0-9a-f]{6}$/.test,
  [Prop.EYE]: ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'].includes
});

const hasAll = (a: boolean, b: boolean) => a && b;

const requiredProperties: Prop[] = Object.values(Prop).filter(prop => prop !== Prop.COUNTRY);
const hasRequiredProperties = (passport: Passport): boolean => requiredProperties.map(prop => passport.has(prop)).reduce(hasAll);

const isValidProperty = (value: string, prop: Prop): boolean => validations.get(prop) ? validations.get(prop)(value) : true;
const isValidPassport = (passport: Passport): boolean => hasRequiredProperties(passport) && passport.map(isValidProperty).reduce(hasAll);

const processPassport = (passportString: string): Passport =>
  Map(passportString.split(/[\n\s]/).filter(input => input).map(property => property.split(':'))) as Passport;

const prepareInput = (input: string): List<Passport> => List(input.split('\n\n')).map(processPassport);

export const day4a = (input: string): number => prepareInput(input).filter(hasRequiredProperties).size;
export const day4b = (input: string): number => prepareInput(input).filter(isValidPassport).size;
