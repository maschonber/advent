import {Repeat} from "immutable";

const matchNumbers = (list: number[], index: number) => list.map((nr, _, list) => ({one: nr, two:list[index]}));

const isEvilNumber = (entryToCheck: number, index: number, allNumbers: number[]): boolean => index >= 25 && !Repeat(allNumbers.slice(index - 25, index), 25)
  .map(matchNumbers).find(list => !!list.find(v => v.one !== v.two && v.one + v.two === entryToCheck));

const minMax = (list: number[]) => Math.min(...list) + Math.max(...list);
const addDimension = (_, index: number, wholeList: number[]) => wholeList.slice(index);

const addUp = (list: number[], target: number) => {
  let sum = 0;
  for (let i = 0; i < list.length && sum < target; i++) {
    if ((sum += list[i]) === target && i > 0) {
      return minMax(list.slice(0, i+1));
    }
  }
}

const prepareInputs = (input: string): number[] => input.split('\n').filter(s => s).map(Number);

export const day9a = (input: string): number => prepareInputs(input).find(isEvilNumber);
export const day9b = (input: string): number => prepareInputs(input).map(addDimension).map(list => addUp(list, day9a(input))).find(v => v);