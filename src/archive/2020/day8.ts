import {Set, Range} from "immutable";

function loop(strings: string[], changing?: number): {acc: number, atEnd: boolean} {
  let visited = Set();
  let acc = 0, pos = 0;

  while (!visited.contains(pos) && pos < strings.length) {
    visited = visited.add(pos);
    const action = getAction(strings[pos]);

    acc += action.op === 'acc' ? action.move : 0;
    pos += (action.op === 'jmp' && changing !== pos) || (action.op === 'nop' && changing === pos) ? action.move : 1;
  }

  return {acc, atEnd: pos >= strings.length - 1};
}

const getAction = (input: string): {op: string, move: number} => {
  const split = input.split(' ');
  return {op: split[0], move: Number(split[1])}
};

const prepareInputs = (input: string): string[] => input.split('\n');

export const day8a = (input: string) => loop(prepareInputs(input)).acc;
export const day8b = (input: string) => Range(0, prepareInputs(input).length + 1).map(nr => loop(prepareInputs(input), nr)).find(result => result.atEnd).acc;
