import {List, Set} from "immutable";

interface Content {color: string, amount: number}
interface BagRule {color: string, content: Content[]}

const sum = (a: number, b: number): number => a + b;

const parseBagRule = (input: string): BagRule => {
  const color = input.match(/(.*) bags contain/)[1];
  const contentPart = input.substr(color.length + 14);
  return {color: color, content: contentPart.includes('no other bags') ? [] : contentPart.split(', ').map(parseContent)};
}

const parseContent = (input: string): Content => {
  const match = input.match(/(\d)+ (.*) bag/);
  return {color: match[2], amount: Number(match[1])}
}

function getContainersHoldingShinyGold(rules: List<BagRule>): number {
  const getContainersOf = (color: string): Set<string> => rules.filter(rule => rule.content.find(c => c.color === color))
    .flatMap(rule => getContainersOf(rule.color).add(rule.color)).toSet();

  return getContainersOf('shiny gold').size;
}

function getBagsInsideShinyGold(rules: List<BagRule>): number {
  const getBagsInside = (color: string): number => rules.find(rule => rule.color === color).content
    .map(content => content.amount + content.amount * getBagsInside(content.color)).reduce(sum, 0);

  return getBagsInside('shiny gold');
}

const prepareInput = (input: string): List<BagRule> => List(input.split('\n').filter(s => s).map(parseBagRule));

export const day7a = (input: string): any => getContainersHoldingShinyGold(prepareInput(input));
export const day7b = (input: string): any => getBagsInsideShinyGold(prepareInput(input));
