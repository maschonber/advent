import {Map} from "immutable";

const prepareInputs = (input: string): number[] => input.split('\n').filter(s => s).map(Number).concat(0);

const getJumps = (nr, index, array) => (array[index + 1] ?? (array[index] + 3)) - nr;

const calculateDifferences = (list: number[]) => list.filter(nr => nr === 1).length * list.filter(nr => nr === 3).length;

const countPossibilities = (list: number[]) => {
  let islands: number[] = [1];
  for (let i = 0; i < list.length; i++) {
    if (list[i] === 1) {
      islands[islands.length - 1]++;
    } else {
      islands = [...islands, 1];
    }
  }
  return islands.map(String).map(str => Map({'1': 1, '2': 1, '3': 2, '4': 4, '5': 7}).get(str)).reduce((a, b) => a * b, 1);
};

export const day10a = (input: string) => calculateDifferences(prepareInputs(input).sort((a, b) => a - b).map(getJumps));
export const day10b = (input: string) => countPossibilities(prepareInputs(input).sort((a, b) => a - b).map(getJumps));
