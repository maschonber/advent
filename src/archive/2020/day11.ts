import {List, Repeat} from "immutable";

const prepareInputs = (input: string): List<string> => List(input.split('\n').filter(s => s));

function countNeighbors(posX: number, posY: number, block: List<List<string>>): number {
  let count = 0;

  for (let y = -1; y < 2; y++) {
    for (let x = -1; x < 2; x++) {
      const xCheck = posX + x, yCheck = posY + y;
      if (!(x === 0 && y === 0) && xCheck > -1 && yCheck > -1 && block.getIn([xCheck, yCheck]) === '#') {
        count++;
      }
    }
  }
  return count;
}

function scanDirection(posX: number, posY: number, x: number, y: number, block: List<List<string>>): boolean {
  for (let i = 1; i < 100; i++) {
    const positionToCheck: string = block.getIn([posX + i * x, posY + i * y]);
    if ((posX + i * x) < 0 || (posY + i * y) < 0 || !positionToCheck || positionToCheck === 'L') {
      return false;
    }
    if (positionToCheck === '#') {
      return true;
    }
  }
  return false;
}

function countNeighborsFar(posX: number, posY: number, block: List<List<string>>): number {
  let count = 0;

  for (let y = -1; y < 2; y++) {
    for (let x = -1; x < 2; x++) {
      if (x !== 0 || y !== 0) {
        count = scanDirection(posX, posY, x, y, block) ? count + 1 : count;
      }
    }
  }
  return count;
}

function changeSeat(block: List<string[]>, x: number, y: number, countFunction: Function, nextStep: List<List<string>>, threshold: number): List<List<string>> {
  if (block.getIn([x, y]) === 'L' && countFunction(x, y, block) < 1) {
    return nextStep.setIn([x, y], '#');
  } else if (block.getIn([x, y]) === '#' && countFunction(x, y, block) >= threshold) {
    return nextStep.setIn([x, y], 'L');
  } else {
    return nextStep.setIn([x, y], block.getIn([x, y]));
  }
}

const progress = (input: List<string>, countFunction: Function, threshold: number): List<string> => {
  const block = List(input.map(row => row.split('')));
  let nextStep: List<List<string>> = Repeat(Repeat(' ', input.get(0).length).toList(), input.size).toList();

  for (let y = 0; y < input.get(0).length; y++) {
    for (let x = 0; x < block.size; x++) {
      nextStep = changeSeat(block, x, y, countFunction, nextStep, threshold);
    }
  }
  return nextStep.map(row => row.join(''));
}

function loop(input: List<string>, countFunction: Function, threshold: number): List<string> {
  let block = input;
  while (!progress(block, countFunction, threshold).equals(block)) {
    block = progress(block, countFunction, threshold); // performance ist ueberbewertet ;)
  }
  return block;
}

const count = (seats: List<string>): number => seats.join('').replace(/[.L]/g, '').length;

export const day11a = (input: string): number => count(loop(prepareInputs(input), countNeighbors, 4));
export const day11b = (input: string): number => count(loop(prepareInputs(input), countNeighborsFar, 5));