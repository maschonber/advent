import {List} from "immutable";

enum Direction {NORTH = 'N', EAST = 'E', SOUTH = 'S', WEST = 'W', FORWARD = 'F', RIGHT = 'R', LEFT = 'L'}

interface State {
  posX: number;
  posY: number;
  facing: Direction;
}

interface Instruction {
  direction: Direction;
  value: number;
}

const initialState: State = {
  posX: 0, posY: 0, facing: Direction.EAST
}

const getManhattan = (state: State) => Math.abs(state.posX) + Math.abs(state.posY);

const getInstruction = (s: string): Instruction => ({direction: s[0] as Direction, value: Number(s.substr(1))});
const prepareInputs = (input: string): List<{direction: Direction, value: number}> => List(input.split('\n').filter(s => s)).map(getInstruction);

const moveEast = (state: State, speed: number): State => ({...state, posX: state.posX + speed});
const moveWest = (state: State, speed: number): State => ({...state, posX: state.posX - speed});
const moveNorth = (state: State, speed: number): State => ({...state, posY: state.posY + speed});
const moveSouth = (state: State, speed: number): State => ({...state, posY: state.posY - speed});

const moveForward = (state: State, speed: number): State => {
  switch (state.facing) {
    case Direction.EAST: return moveEast(state, speed);
    case Direction.WEST: return moveWest(state, speed);
    case Direction.NORTH: return moveNorth(state, speed);
    case Direction.SOUTH: return moveSouth(state, speed);
  }
}

const compass = List([Direction.NORTH, Direction.EAST, Direction.SOUTH, Direction.WEST]);
const turn = (state: State, degree: number, clockwise: boolean): State => {
  const newDirection = compass.get((compass.keyOf(state.facing) + (degree / (clockwise ? 90 : -90))) % 4);

  return {...state, facing: newDirection};
}

export const day12a = (input: string): number => getManhattan(prepareInputs(input).reduce((state: State, instruction: Instruction) => {
  switch (instruction.direction) {
    case Direction.FORWARD: return moveForward(state, instruction.value);
    case Direction.EAST: return moveEast(state, instruction.value);
    case Direction.WEST: return moveWest(state, instruction.value);
    case Direction.NORTH: return moveNorth(state, instruction.value);
    case Direction.SOUTH: return moveSouth(state, instruction.value);
    case Direction.RIGHT: return turn(state, instruction.value, true);
    case Direction.LEFT: return turn(state, instruction.value, false);
    default: throw new Error('undefined direction ' + instruction.direction);
  }
}, initialState));