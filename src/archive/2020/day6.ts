import {List, Set} from 'immutable';

const getPersonAnswers = (groupInput: string): Set<string>[] => groupInput.split('\n').map(person => Set(person.split(''))).filter(s => s.size);
const prepareInput = (input: string): List<Set<string>[]> => List(input.split('\n\n')).map(getPersonAnswers);

const anyoneAnswered = (personAnswers: Set<string>[]): number => personAnswers.reduce((a, b) => a.union(b)).size;
const everyoneAnswered = (personAnswers: Set<string>[]): number => personAnswers.reduce((a, b) => a.intersect(b)).size;

const sum = (a: number, b: number): number => a + b;

export const day6a = (input: string): number => prepareInput(input).map(anyoneAnswered).reduce(sum);
export const day6b = (input: string): number => prepareInput(input).map(everyoneAnswered).reduce(sum);
