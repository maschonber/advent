import {List} from "immutable"

interface PasswordEntry {
    password: string;
    char: string;
    min: number;
    max: number;
}

const onlyKeepTargetChars = (entry: PasswordEntry): PasswordEntry =>
  ({...entry, password: entry.password.replace(new RegExp(`[^${entry.char}]`, 'g'), '')})

const posMatches = (entry: PasswordEntry, pos1Based: number): boolean =>
  entry.password[pos1Based - 1] === entry.char

export function day2a(input: string): number {
    return prepareInput(input)
      .map(entry => onlyKeepTargetChars(entry))
      .filter(entry => entry.password.length >= entry.min && entry.password.length <= entry.max)
      .size
}

export function day2b(input: string): number {
    return prepareInput(input)
      .filter(entry => (posMatches(entry, entry.min) || posMatches(entry, entry.max))
        && !(posMatches(entry, entry.min) && posMatches(entry, entry.max)))
      .size
}

function prepareInput(input: string): List<PasswordEntry> {
    const INPUT_MATCHER = /(\d+)-(\d+) (\w): (.*)/

    return List(input.split('\n')).map(input => input.match(INPUT_MATCHER))
      .filter(matches => !!matches)
      .map(matches => ({min: Number(matches[1]), max: Number(matches[2]), char: matches[3], password: matches[4]}))
}
