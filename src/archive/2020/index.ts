import {day12a} from "./day12";

const fs = require('fs');

fs.readFile('src/inputs20/day12-day1-input.txt', 'utf8', (err, contents) => {
    console.dir(day12a(contents), {'maxArrayLength': null});
});
