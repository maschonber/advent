import {List} from "immutable"

const isTree = (sign: string) => sign === '#';
const isEven = (row: string, i: number) => !(i % 2);

const getTreesForShallowSlope = (input: string, slope: number): number =>
  prepareInput(input).map((row, i) => row[i * slope % row.length]).filter(isTree).size;

const getTreesForSteepSlope = (input: string): number =>
  prepareInput(input).filter(isEven).map((row, i) => row[i % row.length]).filter(isTree).size;

export function day3a(input: string): number {
    return getTreesForShallowSlope(input, 3);
}

export function day3b(input: string): number {
    return [1, 3, 5, 7]
      .map(slope => getTreesForShallowSlope(input, slope))
      .reduce((a, b) => a * b) * getTreesForSteepSlope(input);
}

function prepareInput(input: string): List<string> {
    const strings = List(input.split('\n'))
    return strings.filter(nr => nr);
}
