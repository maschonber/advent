import {List, Repeat} from 'immutable';
import {sum} from '../../util';

export const day6both = (inputString: string): number[] =>
  [grow(prepareInput(inputString), 80).reduce(sum, 0), grow(prepareInput(inputString), 256).reduce(sum, 0)];

const grow = (fish: List<number>, end: number, generation = 0): List<number> => generation >= end ? fish : grow(ageUp(fish), end, generation + 1);
const ageUp = (ages: List<number>) => ages.shift().concat(ages.first()).set(6, ages.get(7) + ages.first());

const prepareInput = (input: string): List<number> => {
  let fish = List<number>(Repeat(0, 9));
  List(input.split(',').map(Number)).forEach(nr => fish = fish.set(nr, fish.get(nr) + 1))
  return fish;
};
