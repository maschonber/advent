import {List, Repeat} from 'immutable';

type Binary = 0 | 1;

export function day3a(inputString: string): number {
  const input = prepareInput(inputString);
  const ones = input.reduce(addUpOnes, List(Repeat(0, 12)));
  const gamma = toDecimal(ones.map((nr) => moreOnes(nr, input.size)));
  const epsilon = toDecimal(ones.map((nr) => lessOnes(nr, input.size)));

  return gamma * epsilon;
}

export function day3b(inputString: string) {
  const input = prepareInput(inputString);
  const oxygen = toDecimal(filterMatching(input, moreOnes, 0).get(0));
  const scrubber = toDecimal(filterMatching(input, lessOnes, 0).get(0));

  return oxygen * scrubber;
}

function filterMatching(input: List<List<Binary>>, keepFunction: (a: number, b: number) => number, indexToCheck: number): List<List<Binary>> {
  if (input.size < 2) {
    return input;
  }
  const onesOnPosition = input.reduce(addUpOnes, List(Repeat(0, 12))).get(indexToCheck);
  const filtered = input.filter(list => list.get(indexToCheck) === keepFunction(onesOnPosition, input.size));

  return filterMatching(filtered, keepFunction, indexToCheck + 1);
}

function prepareInput(input: string): List<List<Binary>> {
  return List(input.split('\n'))
    .filter(string => string.length).map(string => List(string.split('').map(nr => nr === '1' ? 1 : 0)));
}

const toDecimal = (binaries: List<Binary>): number => parseInt(binaries.join(''), 2);

const addUpOnes = (a: List<number>, b: List<number>): List<number> => a.map((nrPos, index) => nrPos + b.get(index));

const moreOnes = (onesOnPosition: number, inputSize: number): Binary => onesOnPosition >= inputSize / 2 ? 1 : 0;
const lessOnes = (onesOnPosition: number, inputSize: number): Binary => onesOnPosition < inputSize / 2 ? 1 : 0;