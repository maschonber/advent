import {List, Map, Set} from 'immutable';

type Graph = Map<string, Set<string>>;
type Paths = Set<List<string>>;
type PathRule = (target: string, path: List<string>) => boolean;

export function day12a(inputString: string): number[] {
  const graph = prepareInput(inputString);
  return [
    iterate(Set.of(List(['start'])), graph, pathRuleA).size,
    iterate(Set.of(List(['start'])), graph, pathRuleB).size
  ];
}

function iterate(paths: Paths, graph: Graph, pathRule: PathRule): Paths {
  if (allPathsFinished(paths)) {
    return paths;
  }
  return iterate(paths.flatMap(path => extendPaths(path, graph, pathRule)), graph, pathRule);
}

const getPossibleTargets = (graph: Map<string, Set<string>>, existingPath: List<string>, pathRule: PathRule) =>
  graph.get(existingPath.get(-1)).filter(target => pathRule(target, existingPath));

function extendPaths(existingPath: List<string>, graph: Graph, pathRule: PathRule): Paths {
  return existingPath.get(-1) === 'end'
    ? Set.of(existingPath)
    : getPossibleTargets(graph, existingPath, pathRule).map(target => existingPath.concat(target));
}

const pathRuleA: PathRule = (target, path) => (isLargeCave(target) || (!path.contains(target)));

const pathRuleB: PathRule = (target, path) =>
  isLargeCave(target) || (smallCaveVisitedTwice(path) ? !path.contains(target) : target !== 'start');

const smallCaveVisitedTwice = (path: List<string>): boolean => {
  const smallCaves = path.filterNot(isLargeCave);
  return smallCaves.size !== Set(smallCaves).size;
}

const isLargeCave = (target: string): boolean => /^[A-Z]/.test(target);

const allPathsFinished = (paths: Set<List<string>>): boolean =>
  paths.size && paths.filter(path => path.get(-1) === 'end').size === paths.size;

function prepareInput(input: string): Graph {
  const edges = List(input.split('\n').filter(x => x).map(line => List(line.split('-'))));
  return edges.reduce((graph, edge) =>
    addEdge(addEdge(graph, edge.get(0), edge.get(1)), edge.get(1), edge.get(0)), Map());
}

const addEdge = (graph: Graph, from: string, to: string): Graph => from === 'end'
  ? graph.set(from, Set()) : graph.set(from, (graph.get(from) ?? Set()).add(to));