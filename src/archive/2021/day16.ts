import {sum} from '../../util';

interface Packet {
  version: number;
  type: number;
  length: number;
  sub: Packet[];
  payload: number;
}

export function day16both(inputString: string): number[] {
  const packet = parsePacket(toBinary(inputString));
  return [countVersions(packet), evaluatePacket(packet)];
}

function countVersions(packet: Packet): number {
  return packet.version + packet.sub.reduce((sum, packet) => sum + countVersions(packet), 0);
}

function evaluatePacket(packet: Packet): number {
  switch (packet.type) {
    case 0: return packet.sub.reduce((sum, b) => sum + evaluatePacket(b), 0);
    case 1: return packet.sub.reduce((prod, b) => prod * evaluatePacket(b), 1);
    case 2: return Math.min(...packet.sub.map(evaluatePacket));
    case 3: return Math.max(...packet.sub.map(evaluatePacket));
    case 4: return packet.payload;
    case 5: return evaluatePacket(packet.sub[0]) > evaluatePacket(packet.sub[1]) ? 1 : 0;
    case 6: return evaluatePacket(packet.sub[0]) < evaluatePacket(packet.sub[1]) ? 1 : 0;
    case 7: return evaluatePacket(packet.sub[0]) === evaluatePacket(packet.sub[1]) ? 1 : 0;
  }
}

function parsePacket(binary: string): Packet {
  const [, versionString, typeString, contentString] = binary.match('^(\\d{3})(\\d{3})(\\d+)');
  const version = parseInt(versionString, 2);
  const type = parseInt(typeString, 2);

  if (type === 4) {
    const {payload, lengthParsed} = parseLiteralContent(contentString);
    return {version, type, payload, sub: [], length: lengthParsed + 6};
  }

  if (contentString.substring(0, 1) === '1') {
    const numberToParse = parseInt(contentString.substring(1, 12), 2);
    const packets = parsePacketsByNumber(contentString.substring(12), numberToParse);
    return {version, type, length: packets.map(p => p.length).reduce(sum, 0) + 6 + 12, sub: packets, payload: undefined};
  } else {
    const lengthToParse = parseInt(contentString.substring(1, 16), 2);
    const packets = parsePacketsByLength(contentString.substring(16), lengthToParse);
    return {version, type, length: packets.map(p => p.length).reduce(sum, 0) + 6 + 16, sub: packets, payload: undefined};
  }
}

const parsePacketsByLength = (binary: string, lengthToParse: number): Packet[] => {
  if (!lengthToParse) {
    return [];
  }

  const packet = parsePacket(binary.substr(0, lengthToParse));
  return [packet, ...parsePacketsByLength(binary.substr(packet.length, lengthToParse), lengthToParse - packet.length)];
};

const parsePacketsByNumber = (binary: string, numberToParse: number): Packet[] =>
  numberToParse ? [parsePacket(binary), ...parsePacketsByNumber(binary.substr(parsePacket(binary).length), numberToParse - 1)] : [];

function parseLiteralContent(literal: string): {payload: number, lengthParsed: number} {
  const {result, lengthParsed} = parseLiteralBlocks(literal);
  return {payload: parseInt(result, 2), lengthParsed};
}

function parseLiteralBlocks(literal: string): {result: string, lengthParsed: number} {
  if (literal.startsWith('1')) {
    const moreBlocks = parseLiteralBlocks(literal.substr(5));
    return {result: literal.substr(1, 4) + moreBlocks.result, lengthParsed: moreBlocks.lengthParsed + 5};
  } else {
    return {result: literal.substr(1, 4), lengthParsed: 5};
  }
}

const toBinary = (input: string): string => input.split('').filter(x => x != '\n').map(char => parseInt(char, 16).toString(2).padStart(4, '0')).join('');
