import {List} from 'immutable';

class Vector {
  constructor(readonly x: number, readonly y: number, readonly aim: number) {}

  public multiply(): number {
    return this.x * this.y;
  }

  toString(): string {
    return this.x + ' ' + this.y + ' ' + this.aim;
  }
}

export const day2a = (input: string): number => prepareInput(input).reduce(move, start).multiply();
export const day2b = (input: string): number => prepareInput(input).reduce(aimedMove, start).multiply();

const move = (a: Vector, b: Vector): Vector => new Vector(a.x + b.x, a.y + b.y, 0);

const aimedMove = (a: Vector, b: Vector): Vector => {
  if (b.x) {
    return new Vector(a.x + b.x, a.y + b.x * a.aim, a.aim);
  }
  if (b.y) {
    return new Vector(a.x, a.y, a.aim + b.y);
  }
}

const start = new Vector(0, 0, 0);

function toVector(row: string): Vector {
  const [direction, speed] = row.split(' ');
  if (direction === 'forward') {
    return new Vector(Number(speed), 0, 0);
  }
  if (direction === 'down') {
    return new Vector(0, Number(speed), 0);
  }
  if (direction === 'up') {
    return new Vector(0, -Number(speed), 0);
  }
}

function prepareInput(input: string): List<Vector> {
  const strings = List(input.split('\n'))
  return strings.filter(s => !!s).map(toVector);
}

