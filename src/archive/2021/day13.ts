import {List, Repeat, Set} from 'immutable';

enum FoldDirection {'X', 'Y'}

interface Input {
  points: Set<string>;
  folds: List<{direction: FoldDirection, position: number}>
}

interface Point {
  x: number;
  y: number;
}

export function day13a(inputString: string): number {
  const {points, folds} = prepareInput(inputString);
  return foldSheet(points, folds.get(0)).size;
}

export function day13b(inputString: string): string {
  const {points, folds} = prepareInput(inputString);

  const foldedPoints = folds.reduce(foldSheet, points).map(stringToPoint);

  const maxX = foldedPoints.sortBy(point => point.x).last().x + 1;
  const maxY = foldedPoints.sortBy(point => point.y).last().y + 1;

  const blankSheet = Repeat(Repeat(' ', maxX).toList(), maxY).toList();
  const password = foldedPoints.reduce((a: List<List<string>>, b: Point) => a.setIn([b.y, b.x], '█'), blankSheet);
  return password.map(row => row.join('')).reduce((line, b) => line.concat('\n' + b), '');
}

const foldSheet = (points, fold) => fold.direction === FoldDirection.X ? foldX(points, fold.position) : foldY(points, fold.position);

const foldX = (points: Set<string>, edge: number): Set<string> => points.map(stringToPoint)
  .map(point => ({x: point.x > edge ? edge - (point.x - edge) : point.x, y: point.y})).map(point => [point.x, point.y].join(','));

const foldY = (points: Set<string>, edge: number): Set<string> => points.map(stringToPoint)
  .map(point => ({y: point.y > edge ? edge - (point.y - edge) : point.y, x: point.x})).map(point => [point.x, point.y].join(','));

const stringToPoint = (point: string): Point => ({x: Number((point.split(','))[0]), y: Number((point.split(','))[1])});

function prepareInput(input: string): Input {
  const parts = input.split('\n\n');
  const points = Set(parts[0].split('\n'));
  const folds = List(parts[1].split('\n').filter(x => x).map(row => {
    const match = row.match(/g (.)=(\d+)/);
    return {direction: (match[1] === 'x') ? FoldDirection.X : FoldDirection.Y, position: Number(match[2])};
  }));
  return {points, folds};
}
