import {List, Range, Repeat} from 'immutable';
import {sum} from '../../util';

export function day7both(inputString: string): number[] {
  const distances = Repeat(prepareInput(inputString), 2000).map((crabs, index) => crabs.map(pos => pos - index).map(Math.abs));
  const fuels = distances.map(possibility => possibility.reduce(sum, 0));
  const fuelsComplex = distances.map(possibility => possibility.map(triangular.get).reduce(sum, 0));

  return [fuels.min(), fuelsComplex.min()];
}

const triangular = Range().map((n) => (n * (n + 1)) / 2); // https://www.mathsisfun.com/algebra/triangular-numbers.html
const prepareInput = (input: string): List<number> => List(input.split(',').map(Number));
