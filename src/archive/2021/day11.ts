import {List} from 'immutable';

type World = List<Point>;

interface State {
  world: World;
  flashes: number;
  gen: number;
}

interface Point {
  x: number;
  y: number;
  level: number;
}

export function day11both(inputString: string): number[] {
  const world = prepareInput(inputString);
  const after100 = iterate({world, flashes: 0, gen: 100});
  const synchronized = iterate({world, flashes: 0, gen: 1000});

  return [after100.flashes, 1000 - synchronized.gen];
}

function iterate(state: State): State {
  if (!state.gen) {
    return state;
  }

  const newState = flash({...state, gen: state.gen - 1, world: state.world.map(point => ({...point, level: point.level + 1}))});

  if (newState.flashes - state.flashes === 100) {
    return newState;
  }

  return iterate(newState);
}

function flash(state: State): State {
  const {world, flashes} = state;
  const nextToFlash = world.findIndex(point => point.level > 9);

  if (nextToFlash > -1) {
    const newWorld = world.set(nextToFlash, {...world.get(nextToFlash), level: 0});
    const neighbors = getNeighbors(world.get(nextToFlash), newWorld);
    const flashed = newWorld.map(point => neighbors.find(p => p.x === point.x && p.y === point.y) && point.level > 0 ? {...point, level: (point.level + 1)} : point);
    return flash({world: flashed, flashes: flashes + 1, gen: state.gen});
  } else {
    return state;
  }
}

function getNeighbors(center: Point, world: World): List<Point> {
  return world.filter(point => point.x >= center.x - 1 && point.x <= center.x + 1 && point.y >= center.y -1 && point.y <= center.y + 1);
}

function prepareInput(input: string): World {
  return List(input.split('\n')
    .filter(x => x)).flatMap(((row, rowIndex) => List(row.split('')
      .map((nr, colIndex) => ({x: colIndex, y: rowIndex, level: Number(nr)})))));
}
