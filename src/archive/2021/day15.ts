import {List} from 'immutable';

type World = Node[][];

interface Node {
  risk: number;
  distance: number;
  visited: boolean;
}

interface Point {
  x: number;
  y: number;
  node: Node;
  neighbors: Node[];
}

export function day15a(inputString: string): number {
  const world = prepareInput(inputString);
  world[0][0].distance = 0;
  const unvisited = getUnvisitedPoints(world);

  return doTheDijkstraDance(world, unvisited);
}

export function day15b(inputString: string): number {
  const world = prepareInputB(inputString);
  world[0][0].distance = 0;
  const unvisited = getUnvisitedPoints(world);

  return doTheDijkstraDance(world, unvisited);
}

function getUnvisitedPoints(world: Node[][]): Point[] {
  let unvisited: Point[] = [];
  for (let y = 0; y < world.length; y++) {
    for (let x = 0; x < world[0].length; x++) {
      unvisited.push({x, y, node: world[y][x], neighbors: getNeighbors(x, y, world)});
    }
  }
  return unvisited;
}

function doTheDijkstraDance(world: Node[][], unvisited: Point[]) {
  while (unvisited.length) {
    unvisited.sort((a, b) => a.node.distance > b.node.distance ? -1 : 1);
    const currentPoint: Point = unvisited.pop();
    const currentNode = world[currentPoint.y][currentPoint.x];
    currentNode.visited = true;
    const neighbors = currentPoint.neighbors;

    neighbors.forEach(neighbor => {
      if (!neighbor.visited && currentNode.distance + neighbor.risk < neighbor.distance) {
        neighbor.distance = currentNode.distance + neighbor.risk;
      }
    })
  }

  return world[world.length-1][world[world.length-1].length-1].distance;
}

function prepareInput(input: string): World {
  const list = List(input.split('\n')).filter(line => !!line).map((line, yIndex) =>
    line.split('').filter(risk => Number(risk) > 0).map((risk, xIndex) =>
      ({x: xIndex, y: yIndex, risk: Number(risk), distance: Infinity, visited: false})));
  return list.toArray();
}

const getRisk = (node: Node, offset: number): number => (node.risk + offset) > 9 ? (node.risk + offset - 9) : node.risk + offset;

function getNeighbors(x: number, y: number, world: World): Node[] {
  const top = y > 0 ? [world[y - 1][x]] : [];
  const left = x > 0 ? [world[y][x - 1]] : [];
  const right = x + 1 < world[y].length ? [world[y][x + 1]] : [];
  const bottom = y + 1 < world.length ? [world[y + 1][x]] : [];

  return [...top, ...left, ...right, ...bottom];
}

function prepareInputB(input: string): World {
  const grid = prepareInput(input);
  let gridBlock: Node[][] = [];

  for (let y = 0; y < grid.length; y++) {
    let bigRow: Node[] = [];
    for (let x = 0; x < 5; x++) {
      let row = grid[y].map(node => ({...node, risk: getRisk(node, x)}));
      bigRow.push(...row);
    }
    gridBlock.push(bigRow);
  }

  let bigGrid: Node[][] = [];
  for (let y = 0; y < 5; y++) {
    gridBlock.map(row => row.map(node => ({...node, risk: getRisk(node, y)}))).forEach(row => {
      bigGrid.push(row);
    })
  }

  return bigGrid;
}
