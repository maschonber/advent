import {List} from 'immutable';
import {sum} from '../../util';

export function day1a(input: string): number {
    return prepareInput(input)
      .map(greaterThanPrevious)
      .reduce(sum);
}

export function day1b(input: string): number {
    return prepareInput(input)
    .map(addPreviousDepths)
    .slice(2)
    .map(greaterThanPrevious)
    .reduce(sum);
}

function prepareInput(input: string): List<number> {
  const strings = List(input.split('\n'))
  return strings.map(string => Number(string)).filter(nr => nr > 0);
}

const addPreviousDepths = (nr: number, index: number, array: List<number>): number => array.slice(index - 2, index + 1).reduce(sum);

const greaterThanPrevious = (nr: number, index: number, array: List<number>) => (index > 0 && nr > array.get(index - 1)) ? 1 : 0;
