import {List} from 'immutable';
import {sum} from '../../util';

interface Input {
  draw: List<number>;
  boards: List<List<List<number>>>;
}

export function day4Both(inputString: string): Array<number> {
  const {draw, boards} = prepareInput4(inputString);

  const results = boards.map(board => draw.map((v, i) => draw.slice(0, i)).map(order => check(board, order)))
    .map(result => result.find(s => !!s)).sortBy(a => a.drawn).map(res => res.score);

  return [results.first(), results.last()];
}

export function prepareInput4(input: string): Input {
  const blocks = List(input.split('\n\n'));

  return {
    draw: List(blocks.get(0).split(',').map(nr => Number(nr))),
    boards: blocks.splice(0, 1).map((block) =>
      List(block.split('\n').map(line => List(line.split(' ').map(nr => Number(nr)))))
    )
  }
}

const includesWinner = (candidate: List<List<number>>, draw: List<number>): boolean => candidate.map(line => line.filterNot(nr => draw.includes(nr))).some(list => list.isEmpty());
const score = (draw: List<number>, numbers: List<List<number>>): number => numbers.map(row => row.filterNot(nr => draw.includes(nr)).reduce(sum, 0)).reduce(sum, 0) * draw.last();
const check = (numbers: List<List<number>>, draw: List<number>): {drawn, score} => (includesWinner(numbers, draw) || includesWinner(transpose(numbers), draw)) ? {drawn: draw.size, score: score(draw, numbers)} : undefined;
const transpose: (list: List<List<any>>) => List<List<any>> = m => m.get(0).map((x, i) => m.map(x => x.get(i)));
