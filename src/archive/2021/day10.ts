import {fromJS, List, Map} from 'immutable';
import {sum} from '../../util';

const corruptedScoreMap: Map<string, number> = fromJS({')': 3, ']': 57, '}': 1197, '>': 25137});
const completionScoreMap: Map<string, number> = fromJS({'(': 1, '[': 2, '{': 3, '<': 4});

export function day10a(inputString: string): number {
  const corrupted = prepareInput(inputString).map(eliminateClosed)
    .filter(corruptedMatch).map(findCorruptedChar).filter(x => x);
  return corrupted.map(char => corruptedScoreMap.get(char)).reduce(sum, 0);
}

export function day10b(inputString: string): number {
  const incomplete = prepareInput(inputString).map(eliminateClosed)
    .filter(line => !corruptedMatch(line));
  const incompleteScores = getIncompleteScores(incomplete);
  return incompleteScores.sort().get(incompleteScores.size / 2);
}

const getIncompleteScores = (incomplete: List<string>) =>
  incomplete.map(line => line.split('').reverse().reduce(scoreAutoCompletion, 0));

const scoreAutoCompletion = (a: number, b: string) => a * 5 + completionScoreMap.get(b);

const corruptedMatch = (shrunk: string): RegExpMatchArray => shrunk?.match(/[})>\]]/);
const findCorruptedChar = (shrunk): string => corruptedMatch(shrunk)[0];

function eliminateClosed(input: string): string {
  const reduced = input.replace(/\[]|{}|\(\)|<>/g, '');
  return reduced.length === input.length ? reduced : eliminateClosed(reduced);
}

function prepareInput(input: string): List<string> {
  return List(input.split('\n').filter(x => x));
}
