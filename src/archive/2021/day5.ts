import {List, Repeat} from 'immutable';
import {sum} from '../../util';

const WORLD_SIZE = 1000;

export function day5a(inputString: string): number {
  const vents = prepareInput5(inputString);
  const hundredsOfWorlds = getHorizontal(vents).concat(getVertical(vents));
  const combinedWorld = hundredsOfWorlds.reduce((a, b) => combineWorlds(a, b), emptyWorld());

  return countDense(combinedWorld);
}

export function day5b(inputString: string): number {
  const vents = prepareInput5(inputString);
  const fiveHundredWorlds = getHorizontal(vents).concat(getVertical(vents)).concat(getDiagonal(vents));

  // crunch crunch - this is not efficient, but fun.
  const combinedWorld = fiveHundredWorlds.reduce((a, b) => combineWorlds(a, b), emptyWorld());

  return countDense(combinedWorld);
}

function formatStraightVent(line1: number, line2: number, position: number): StraightVent {
  const {start, end} = line1 < line2 ? {start: line1, end: line2 + 1} : {start: line2, end: line1 + 1};
  const line = List(Repeat(0, start).concat(Repeat(1, end - start)).concat(Repeat(0, WORLD_SIZE - end)));

  return {line, position}
}

function formatDiagonalVent(vent: VentInput): DiagonalVent {
  const right = vent.x2 > vent.x1;
  const downwards = right ? vent.y2 > vent.y1 : vent.y1 > vent.y2;
  const startX = right ? vent.x1 : vent.x2;
  const startY = right ? downwards ? vent.y1 : vent.y2 : downwards ? vent.y2 : vent.y1;
  const length = right ? vent.x2 - vent.x1 + 1 : vent.x1 - vent.x2 + 1;

  return {startX, startY, length, downwards};
}

function horizontal(vent: StraightVent): World {
  return emptyWorld().set(vent.position, vent.line);
}

function vertical(vent: StraightVent): World {
  return emptyWorld().map((line, index) => line.set(vent.position, vent.line.get(index)));
}

function diagonal(vent: DiagonalVent): World {
  return emptyWorld(vent.startY, WORLD_SIZE)
    .concat(emptyWorld(vent.length, WORLD_SIZE).map((line, index) =>
      line.set(vent.startX + (vent.downwards ? index : vent.length - 1 - index), 1)))
    .concat(emptyWorld(WORLD_SIZE - vent.length - vent.startY, WORLD_SIZE));
}

const getHorizontal = (vents: List<VentInput>) => vents.filter(vent => vent.y1 === vent.y2).map(vent => formatStraightVent(vent.x1, vent.x2, vent.y1)).map(v => horizontal(v));
const getVertical = (vents: List<VentInput>) => vents.filter(vent => vent.x1 === vent.x2).map(vent => formatStraightVent(vent.y1, vent.y2, vent.x1)).map(v => vertical(v));
const getDiagonal = (vents: List<VentInput>) => vents.filter(vent => vent.x1 !== vent.x2 && vent.y1 !== vent.y2).map(v => formatDiagonalVent(v)).map(v => diagonal(v));

const countDense: (list: World) => number = list => list.map(line => line.filter(nr => nr >= 2).count()).reduce(sum, 0);

function prepareInput5(input: string): List<VentInput> {
  return List(input.split('\n')).filter(string => string.length)
    .map(string => string.split(/\D/).filter(s => s.length))
    .map(([x1, y1, x2, y2]) => ({x1: Number(x1), y1: Number(y1), x2: Number(x2), y2: Number(y2)}));
}

const combineWorlds = (a: World, b: World): World => a.map((list, index) => combineLists(list, b.get(index)));
const combineLists = (a: List<number>, b: List<number>): List<number> => a.map((nr, index) => a.get(index) + b.get(index));

const emptyWorld = (lines = WORLD_SIZE, rows = WORLD_SIZE) => List(Repeat(List(Repeat(0, rows)), lines));

interface VentInput {
  x1: number;
  x2: number;
  y1: number;
  y2: number;
}

type World = List<List<number>>;

interface StraightVent {
  line: List<number>;
  position: number;
}

interface DiagonalVent {
  startX: number;
  startY: number;
  length: number;
  downwards: boolean;
}
