import {List, Map} from 'immutable';
import {sum} from '../../util';

interface Pair {
  amount: number;
  first: string;
  second: string;
}

type Polymers = Map<string, Pair>;
type LetterCount = Map<string, number>;

function countLetters(result: Map<string, Pair>, lastLetter: string): LetterCount {
  const groupedByNumber = groupByNumber(result);
  return groupedByNumber.set(lastLetter, groupedByNumber.get(lastLetter) + 1).sort();
}

export function day14both(inputString: string): number[] {
  const {polymers, lastLetter} = prepareInput(inputString);
  const letters10 = countLetters(polymerize(polymers, 10), lastLetter);
  const letters40 = countLetters(polymerize(polymers, 40), lastLetter);
  return [(mostMinusLeast(letters10)), mostMinusLeast(letters40)];
}

function polymerize(polymers: Polymers, target: number, gen = 0): Polymers {
  if (gen >= target) {
    return polymers;
  }

  let nextGen = polymers.map(group => ({...group, amount: 0}));
  polymers.forEach(group => {
    const newAmountFirst = nextGen.get(group.first).amount + group.amount;
    const newAmountSecond = nextGen.get(group.second).amount + group.amount;
    const withFirst = nextGen.setIn([group.first, 'amount'], newAmountFirst);
    nextGen = withFirst.setIn([group.second, 'amount'], newAmountSecond);
  });
  return polymerize(nextGen, target, ++gen);
}

const groupByNumber = (result: Polymers): LetterCount =>
  Map(result.map(value => value.amount).groupBy((value, key) => key.substring(0, 1))
    .map(value => value.reduce(sum, 0)));

const mostMinusLeast = (letters: LetterCount): number => letters.last() - letters.first();

function prepareInput(input: string): { polymers: Polymers, lastLetter: string } {
  const lines = input.split('\n');
  const template = lines[0];
  return {
    lastLetter: template.substring(template.length - 1), polymers: List(lines).splice(0, 2).filter(x => x)
      .reduce((rows, line) => {
        const key = line.substring(0, 2);
        const insertion = line.substring(line.length - 1, line.length);
        const first = key[0] + insertion;
        const second = insertion + key[1];
        const amount = template.match(new RegExp(key, 'g'))?.length || 0;

        return rows.set(key, {amount, first, second});
      }, Map<string, Pair>())
  };
}
