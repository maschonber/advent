import {List, Set} from 'immutable';
import {mult, sum} from '../../util';

type World = List<List<Point>>;

interface Point {
  x: number;
  y: number;
  height: number;
}

export function day9both(inputString: string): number[] {
  const heightMap = prepareInput(inputString);

  const lowPoints = heightMap.flatMap(row => row.filter(point => isLowPoint(point, heightMap)));
  const basins = lowPoints.map(point => spread(point, heightMap));

  const basinScore = getBiggestBasins(basins).map(basin => basin.count()).reduce(mult, 1);
  const lowPointScore = lowPoints.map(point => point.height + 1).reduce(sum, 0);

  return [lowPointScore, basinScore];
}

const isLowPoint = (point: Point, heightMap: World): boolean =>
  point.height < getNeighbors(point, heightMap).map(point => point.height).min();

const getBiggestBasins = (basins: List<Set<Point>>) =>
  basins.sortBy(basin => basin.size).reverse().take(3);

const getHigherNeighbors = (low: Point, heightMap: List<List<Point>>) =>
  getNeighbors(low, heightMap).filter(neighbor => neighbor.height > low.height && neighbor.height < 9);

const spread = (low: Point, heightMap: World): Set<Point> =>
  Set.of(low).concat(getHigherNeighbors(low, heightMap).flatMap(neighbor => spread(neighbor, heightMap)));

function getNeighbors(point: Point, heightMap: World): List<Point> {
  const top = point.y > 0 ? [heightMap.get(point.y-1).get(point.x)] : [];
  const left = point.x > 0 ? [heightMap.get(point.y).get(point.x - 1)] : [];
  const right = point.x + 1 < heightMap.get(point.y).size ? [heightMap.get(point.y).get(point.x + 1)] : [];
  const bottom = point.y + 1 < heightMap.size ? [heightMap.get(point.y + 1).get(point.x)] : [];

  return List([...top, ...left, ...right, ...bottom]);
}

function prepareInput(input: string): World {
  return List(input.split('\n')
    .filter(x => x)
    .map((row, rowIndex) => List(row.split('')
      .map((nr, colIndex) => ({x: colIndex, y: rowIndex, height: Number(nr)})))));
}
