import {List} from 'immutable';
import {sum} from '../../util';

interface Input {
  signalPattern: List<string>,
  outputValue: List<string>
}

export const day8a = (inputString: string): number => prepareInput(inputString).map(input => input.outputValue).map(countSimples).reduce(sum, 0);
export const day8b = (inputString: string): number => prepareInput(inputString).map(line => decipher(getCode(line.signalPattern), line.outputValue)).reduce(sum, 0);

function getCode(pattern: List<string>): string[] {
  const one = pattern.find(digit => digit.length === 2);
  const seven = pattern.find(digit => digit.length === 3);
  const four = pattern.find(digit => digit.length === 4);
  const eight = pattern.find(digit => digit.length === 7);
  const six = pattern.find(digit => digit.length === 6 && !allIncluded(one, digit));
  const five = pattern.find(digit => digit.length === 5 && allIncluded(digit, six));
  const three = pattern.find(digit => digit.length === 5 && allIncluded(one, digit));
  const nine = pattern.find(digit => digit.length === 6 && digit !== six && allIncluded(five, digit));
  const two = pattern.find(digit => digit.length === 5 && digit !== five && digit !== three);
  const zero = pattern.find(digit => digit.length === 6 && digit !== six && digit !== nine);

  return [zero, one, two, three, four, five, six, seven, eight, nine];
}

const allIncluded = (smaller: string, bigger: string): boolean => List(smaller.split('')).every(char => bigger.includes(char));

const decipher = (code: string[], signalOutput: List<string>): number => Number(signalOutput.map(nr => code.indexOf(nr).toString()).join(''));

const countSimples = digits => digits.filter(digit => digit.length <= 4 || digit.length === 7).count();

function prepareInput(input: string): List<Input> {
  return List(input.split('\n').filter(x => x).map(line => {
    const [signalString, outputString] = line.split('|');
    return {signalPattern: splitInput(signalString), outputValue: splitInput(outputString) }
  }));
}

const splitInput: (numbers: string) => List<string> = numbers =>
  List(numbers.split(' ').filter(x => x).map(str => str.split('').sort((a, b) => a.localeCompare(b)).join('')));