import {List, Range, Repeat} from "immutable";

export function day3a(input: string) {
    const number = parseInt(input)
    const oddSquares = Range(1).take(number).filter(value => value % 2).map(value => value * value)
    const lowerBound = oddSquares.filter(value => value < number).last()
    const upperBound = oddSquares.filter(value => value > number).first()
    const toGoInward = oddSquares.indexOf(upperBound)
    const toWalkFromCorner = Range(0, (upperBound - lowerBound) / 8 + 1)
    const ring = Range(lowerBound + 1, upperBound)
    const edgeWalk = toWalkFromCorner.reverse().rest().concat(toWalkFromCorner.rest())
    const ringWalk = Range(0, 4).map(() => edgeWalk).flatten()
    const toGoSideways = ringWalk.get(ring.indexOf(number))
    return toGoSideways + toGoInward
}



export function day3b(input: string) {
    const inputAsNumber = parseInt(input)
    const coords = Range(0, 30).flatMap(coordsForRing).toList();
    let valueMap: Array<Coords> = coords.toJS();

    for (let i = 1; i < valueMap.length; i++) {
        valueMap[i].value = findAdjacent(valueMap[i], valueMap).map(v => v.value).reduce((a, b) => a + b, 0);
    }
    return valueMap.map(coords => coords.value).find(value => value > inputAsNumber);
}

function findAdjacent(coords: Coords, allCoords: Array<Coords>): Array<Coords> {
    return allCoords.filter(c => c.x >= coords.x - 1 && c.x <= coords.x + 1 && c.y >= coords.y - 1 && c.y <= coords.y + 1 &&
        !(c.x === coords.x && c.y === coords.y))
}

function coordsForRing(x: number): List<Coords> {
    if (!x) {
        return List.of({x: 0, y: 0, value: 1});
    }

    const edgeLength = x * 2
    const xMoveUp = Repeat(x, edgeLength)
    const yMoveUp = Range(0, edgeLength).map(value => value - (x - 1))
    const moveUp = xMoveUp.zipWith((x, y) => ({x, y, value: 0}), yMoveUp);

    const xMoveLeft = Range(edgeLength, 0).map(value => value - (x + 1))
    const yMoveLeft = Repeat(xMoveUp.last(), edgeLength)
    const moveLeft = xMoveLeft.zipWith((x, y) => ({x, y, value: 0}), yMoveLeft)

    const xMoveDown = Repeat(xMoveLeft.last(), edgeLength)
    const yMoveDown = Range(edgeLength, 0).map(value => value - (x + 1))
    const moveDown = xMoveDown.zipWith((x, y) => ({x, y, value: 0}), yMoveDown);

    const xMoveRight = Range(0, edgeLength).map(value => value - (x - 1))
    const yMoveRight = Repeat(yMoveDown.last(), edgeLength)
    const moveRight = xMoveRight.zipWith((x, y) => ({x, y, value: 0}), yMoveRight)

    return moveUp.concat(moveLeft).concat(moveDown).concat(moveRight).toList()
}

interface Coords {
    x: number;
    y: number;
    value?: number;
}