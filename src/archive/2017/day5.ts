import {List} from "immutable";

export function day5a(input: string) {
    const jumps = prepareInput(input)
    let state = {jumps, position: 0, steps: 0};

    while (true) {
        if (state.position >= state.jumps.size) {
            return state.steps
        }
        state = jump(state, increaseByOne);
    }
}

export function day5b(input: string) {
    const jumps = prepareInput(input)
    let state = {jumps, position: 0, steps: 0};

    while (true) {
        if (state.position >= state.jumps.size) {
            return state.steps
        }
        state = jump(state, decreaseIfGreater3);
    }
}

const increaseByOne = (value: number) => value +1;
const decreaseIfGreater3 = (value: number) => value >=3 ? value -1 : value +1;

function prepareInput(input: string): List<number> {
    return List(input.split('\n')).map(line => parseInt(line.trim()))
}

function jump(state: State, jumpRule: (number) => number): State {
    const newPosition = state.position + state.jumps.get(state.position)
    const newJumps = state.jumps.update(state.position, jumpRule)

    return {jumps: newJumps, position: newPosition, steps: state.steps+1}
}

interface State {
    jumps: List<number>,
    position: number,
    steps: number
}