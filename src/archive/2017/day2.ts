import {List} from "immutable";

export function day2a(input: string): number {
    const matrix = prepareInput(input);

    const mins = matrix.map(row => row.min())
    const maxs = matrix.map(row => row.max())
    const diffs: List<number> = maxs.zipWith((max, min) => max - min, mins)
    return diffs.reduce((a, b) => a + b, 0)
}

export function day2b(input: string) {
    const matrix = prepareInput(input);
    const sorted = matrix.map(row => row.sort((a, b) => a < b ? 1 : -1))
    const dividers = sorted.map(divider)
    return dividers.reduce((a, b) => a + b, 0)
}

function prepareInput(input: string): List<List<number>> {
    return List(input.split('\n'))
        .map(row => List(row.split('\t'))
            .map(item => parseInt(item.trim())));
}

function divider(sortedList: List<number>): number {
    const first = sortedList.first();
    const rest = sortedList.rest();
    const divided = rest.map(number => first % number)
    const indexOfDivider = divided.indexOf(0)
    return indexOfDivider > -1 ? first / sortedList.get(indexOfDivider + 1) : divider(rest);
}