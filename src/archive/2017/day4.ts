import {List} from "immutable";

export function day4a(input: string): number {
    const phrases = prepareInput(input);
    return phrases.filter(words => words.size === words.toSet().size).size
}

export function day4b(input: string): number {
    const phrases = prepareInput(input);
    const sorted = phrases.map(words => words.map(string =>
        List.of(string.split('')).first().sort()
            .reduce((a: string, b: string) => a + '' + b)))
    return sorted.filter(words => words.size === words.toSet().size).size
}

function prepareInput(input: string): List<List<string>> {
    return List(input.split('\n'))
        .map(row => List(row.split(' ').map(word => word.trim())));
}
