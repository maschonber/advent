import {List} from 'immutable';
import {sum} from './util';

interface Round {
  enemy: number,
  yours: number
}

export const day2a = (input: string): number => prepareInput(input).map(scoreRound).reduce(sum);
export const day2b = (input: string): number => prepareInput(input).map(chooseMove).map(scoreRound).reduce(sum);

const normalize = (nr: number): number => (3 + nr) % 3;
const scoreRound = (round: Round): number => [3, 0, 6][normalize(round.enemy - round.yours)] + round.yours + 1;
const chooseMove = (round: Round): Round => ({...round, yours: normalize(round.enemy + round.yours - 1)});

const prepareInput = (input: string): List<Round> => List(input.split('\n').filter(s => s)
  .map(string => ({enemy: string.charCodeAt(0) - 65, yours: string.charCodeAt(2) - 88})));
