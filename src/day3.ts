import {List, Set} from 'immutable';
import {sum} from './util';

export function day3a(input: string): number {
  return prepareInput(input).map(sharedItem).map(scoreItem).reduce(sum);
}

export function day3b(input: string): number {
  return findElfGroups(prepareInput(input)).map(sharedItemInGroup).map(scoreItem).reduce(sum);
}

const findElfGroups = (allElfs: List<string>): List<List<string>> =>
  allElfs.reduce((a, _, index) => {
    return (index % 3) ? a : a.push(List.of(allElfs.get(index), allElfs.get(index + 1), allElfs.get(index + 2)));
  }, List<List<string>>());

const sharedItemInGroup = (elfs: List<string>): string => {
  const sets = elfs.map(elf => Set(elf.split('')));
  return sets.get(0).intersect(sets.get(1), sets.get(2)).first();
};

const sharedItem = (input: string): string => Set(input.slice(0, input.length / 2).split(''))
  .intersect(Set(input.slice(input.length / 2, input.length).split(''))).first();

const scoreItem = (character: string): number => character.charCodeAt(0) > 90 ? character.charCodeAt(0) - 96 : character.charCodeAt(0) - 38;

const prepareInput = (input: string): List<string> => List(input.split('\n').filter(s => s));
