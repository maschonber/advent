import {day10a} from './day10';

const fs = require('fs');

fs.readFile('src/inputs22/day10-input.txt', 'utf8', (err, contents) => {
  console.dir(day10a(contents), {'maxArrayLength': null});
});
