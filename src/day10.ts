import {List} from 'immutable';
import {sum} from './util';

interface Action {
  type: string;
  payload?: number;
}

interface Entry {
  cycle: number;
  value: number;
}

const toCheck = [20, 60, 100, 140, 180, 220].map(x => x - 1);

export function day10a(input: string): number {
  const result = prepareInput(input).reduce(reducerFunction, List.of({cycle: 0, value: 1}));

  for (let i = 0; i < 6; i++) {
    const screen = result.slice(i * 40, i * 40 + 40).map((entry, nr) => [entry.value-1, entry.value, entry.value+1].includes(nr) ? '#' : '.');
    console.log(screen.join(''));
  }

  const samples = result.filter(entry => toCheck.includes(entry.cycle)).map(entry => (entry.cycle + 1) * entry.value);
  return samples.reduce(sum);
}

function reducerFunction(state: List<Entry>, action: Action): List<Entry> {
  const last = state.last();

  if (action.type === 'noop') {
    return state.push({cycle: last.cycle + 1, value: last.value});
  }
  if (action.type === 'addx') {
    const intermediate = state.push({cycle: last.cycle + 1, value: last.value});
    return intermediate.push({cycle: last.cycle + 2, value: last.value + action.payload});
  }

  return state;
}

const prepareInput = (input: string): List<Action> => List(input.split('\n').filter(s => s).map(string => {
  const parts = string.split(' ');
  return {type: parts[0], payload: Number(parts[1]) || undefined};
}));
